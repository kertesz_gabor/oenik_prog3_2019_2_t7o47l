﻿// <copyright file="MovieRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyMDB.Data;

    /// <summary>
    /// Provides basic functionalities for the purpose of modifying the movies table of the database or obtaining information from it.
    /// </summary>
    public class MovieRepository : IRepository<MOVIE>
    {
        private MyMDB_databaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="MovieRepository"/> class.
        /// </summary>
        public MovieRepository()
        {
            this.db = MovieDatabase.MovieDb;
        }

        /// <summary>
        /// Saves a movie instance in the database.
        /// </summary>
        /// <param name="movie">A new movie.</param>
        public void CreateEntity(MOVIE movie)
        {
            this.db.MOVIES.Add(movie);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a movie instance from the database.
        /// </summary>
        /// <param name="id">The id of the movie to delete.</param>
        public void DeleteEntity(int id)
        {
            this.db.MOVIES.Remove(this.GetEntity(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns all elements of the table Movies.
        /// </summary>
        /// <returns>Movie type queriable collection.</returns>
        public IQueryable<MOVIE> GetAllEntities()
        {
            return this.db.MOVIES as IQueryable<MOVIE>;
        }

        /// <summary>
        /// Returns a specific element of the table Movies.
        /// </summary>
        /// <returns>Movie.</returns>
        /// <param name="id">The id of the desired element.</param>
        public MOVIE GetEntity(int id)
        {
            return this.db.MOVIES.Where(t => t.MOVIEID == id).FirstOrDefault();
        }

        /// <summary>
        /// Updates a movie given its id.
        /// </summary>
        /// <param name="id">The id of the desired movie.</param>
        /// <param name="newMovie">The new movie.</param>
        public void UpdateEntity(int id, MOVIE newMovie)
        {
            newMovie.MOVIEID = id;
            var oldMovie = this.GetEntity(id);
            this.db.Entry(oldMovie).CurrentValues.SetValues(newMovie);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns the last used id in the current table thus offering help when a new entity needs an id.
        /// </summary>
        /// <returns>An id.</returns>
        public int GetLastId()
        {
            return (int)this.GetAllEntities().Max(t => t.MOVIEID);
        }
    }
}
