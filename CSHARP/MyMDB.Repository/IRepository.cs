﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// An interface to provide fundamental db abilities for further repositories implementing.
    /// </summary>
    /// <typeparam name="T">A type of repository.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Saves an entity in the database.
        /// </summary>
        /// <param name="entityToCreate">An entity to create.</param>
        void CreateEntity(T entityToCreate);

        /// <summary>
        /// Updates an entity instance in the database.
        /// </summary>
        /// <param name="id">The id of the entity to change.</param>
        /// <param name="newEntity">A new entity.</param>
        void UpdateEntity(int id, T newEntity);

        /// <summary>
        /// Returns all elements of the database table.
        /// </summary>
        /// <returns>T type queryable collection.</returns>
        IQueryable<T> GetAllEntities();

        /// <summary>
        /// Returns a specific element of the database table.
        /// </summary>
        /// <returns>T.</returns>
        /// <param name="id">The id of the desired element.</param>
        T GetEntity(int id);

        /// <summary>
        /// Deletes a specific element of the database table.
        /// </summary>
        /// <param name="id">The id of the desired element to delete.</param>
        void DeleteEntity(int id);

        /// <summary>
        /// Returns the last used id in the current table thus offering help when a new entity needs an id.
        /// </summary>
        /// <returns>An id.</returns>
        int GetLastId();
    }
}
