﻿// <copyright file="ReviewRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyMDB.Data;

    /// <summary>
    /// Provides basic functionalities for the purpose of modifying the Reviews table of the database or obtaining information from it.
    /// </summary>
    public class ReviewRepository : IRepository<REVIEW>
    {
        private MyMDB_databaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReviewRepository"/> class.
        /// </summary>
        public ReviewRepository()
        {
            this.db = MovieDatabase.MovieDb;
        }

        /// <summary>
        /// Saves a review instance in the database.
        /// </summary>
        /// <param name="review">A new review.</param>
        public void CreateEntity(REVIEW review)
        {
            this.db.REVIEWS.Add(review);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a review instance from the database.
        /// </summary>
        /// <param name="id">The id of the review to delete.</param>
        public void DeleteEntity(int id)
        {
            this.db.REVIEWS.Remove(this.GetEntity(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns all elements of the table Reviews.
        /// </summary>
        /// <returns>Review type queriable collection.</returns>
        public IQueryable<REVIEW> GetAllEntities()
        {
            return this.db.REVIEWS as IQueryable<REVIEW>;
        }

        /// <summary>
        /// Returns a specific element of the table Reviews.
        /// </summary>
        /// <returns>Review.</returns>
        /// <param name="id">The id of the desired element.</param>
        public REVIEW GetEntity(int id)
        {
            return this.db.REVIEWS.Where(t => t.REVIEWID == id).FirstOrDefault();
        }

        /// <summary>
        /// Updates a Review record given its id.
        /// </summary>
        /// <param name="id">The id of the desired review.</param>
        /// <param name="newReview">The new review.</param>
        public void UpdateEntity(int id, REVIEW newReview)
        {
            newReview.REVIEWID = id;
            var oldReview = this.GetEntity(id);
            this.db.Entry(oldReview).CurrentValues.SetValues(newReview);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns the last used id in the current table thus offering help when a new entity needs an id.
        /// </summary>
        /// <returns>An id.</returns>
        public int GetLastId()
        {
            var maxID = this.GetAllEntities().Max(t => (decimal?)t.REVIEWID);
            if (maxID == null)
            {
                return 0;
            }
            else
            {
                return (int)maxID;
            }
        }
    }
}
