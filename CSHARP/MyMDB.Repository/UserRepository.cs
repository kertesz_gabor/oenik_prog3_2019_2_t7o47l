﻿// <copyright file="UserRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyMDB.Data;

    /// <summary>
    /// Provides basic functionalities for the purpose of modifying the Users table of the database or obtaining information from it.
    /// </summary>
    public class UserRepository : IRepository<USER>
    {
        private MyMDB_databaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        public UserRepository()
        {
            this.db = MovieDatabase.MovieDb;
        }

        /// <summary>
        /// Saves a user instance in the database.
        /// </summary>
        /// <param name="user">A new user.</param>
        public void CreateEntity(USER user)
        {
            this.db.USERS.Add(user);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes a user instance from the database.
        /// </summary>
        /// <param name="id">The id of the user to delete.</param>
        public void DeleteEntity(int id)
        {
            this.db.USERS.Remove(this.GetEntity(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns all elements of the table Users.
        /// </summary>
        /// <returns>User type queriable collection.</returns>
        public IQueryable<USER> GetAllEntities()
        {
            return this.db.USERS as IQueryable<USER>;
        }

        /// <summary>
        /// Returns a specific element of the table Users.
        /// </summary>
        /// <returns>User.</returns>
        /// <param name="id">The id of the desired element.</param>
        public USER GetEntity(int id)
        {
            return this.db.USERS.Where(t => t.USERID == id).FirstOrDefault();
        }

        /// <summary>
        /// Updates a User record given its id.
        /// </summary>
        /// <param name="id">The id of the desired user.</param>
        /// <param name="newUser">The new user.</param>
        public void UpdateEntity(int id, USER newUser)
        {
            newUser.USERID = id;
            var oldUser = this.GetEntity(id);
            this.db.Entry(oldUser).CurrentValues.SetValues(newUser);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns the last used id in the current table thus offering help when a new entity needs an id.
        /// </summary>
        /// <returns>An id.</returns>
        public int GetLastId()
        {
            return (int)this.GetAllEntities().Max(t => t.USERID);
        }
    }
}
