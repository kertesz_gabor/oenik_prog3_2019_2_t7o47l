﻿// <copyright file="ActorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyMDB.Data;

    /// <summary>
    /// Provides basic functionalities for the purpose of modifying the Actors table of the database or obtaining information from it.
    /// </summary>
    public class ActorRepository : IRepository<ACTOR>
    {
        private MyMDB_databaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActorRepository"/> class.
        /// </summary>
        public ActorRepository()
        {
            this.db = MovieDatabase.MovieDb;
        }

        /// <summary>
        /// Saves an actor instance in the database.
        /// </summary>
        /// <param name="actor">A new actor.</param>
        public void CreateEntity(ACTOR actor)
        {
            this.db.ACTORS.Add(actor);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes an actor instance from the database.
        /// </summary>
        /// <param name="id">The id of the actor to delete.</param>
        public void DeleteEntity(int id)
        {
            this.db.ACTORS.Remove(this.GetEntity(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns all elements of the table Actors.
        /// </summary>
        /// <returns>Actor type queriable collection.</returns>
        public IQueryable<ACTOR> GetAllEntities()
        {
            return this.db.ACTORS as IQueryable<ACTOR>;
        }

        /// <summary>
        /// Returns a specific element of the table Actors.
        /// </summary>
        /// <returns>Actor.</returns>
        /// <param name="id">The id of the desired element.</param>
        public ACTOR GetEntity(int id)
        {
            return this.db.ACTORS.Where(t => t.ACTORID == id).FirstOrDefault();
        }

        /// <summary>
        /// Returns the last used id in the current table thus offering help when a new entity needs an id.
        /// </summary>
        /// <returns>An id.</returns>
        public int GetLastId()
        {
            return (int)this.GetAllEntities().Max(t => t.ACTORID);
        }

        /// <summary>
        /// Updates an Actor record given its id.
        /// </summary>
        /// <param name="id">The id of the desired actor.</param>
        /// <param name="newActor">The new actor.</param>
        public void UpdateEntity(int id, ACTOR newActor)
        {
            newActor.ACTORID = id;
            var oldActor = this.GetEntity(id);
            this.db.Entry(oldActor).CurrentValues.SetValues(newActor);
            this.db.SaveChanges();
        }
    }
}
