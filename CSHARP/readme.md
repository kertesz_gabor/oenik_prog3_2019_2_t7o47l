Projektn�v: MyMDB 
Le�r�s: 
A MyMDB egy filmes adatb�zis, ahol a userek a rendszerben tal�lhat� filmeket �rt�kelhetnek, adhatnak hozz� egy�ni
list�khoz, valamint sz�veges �rt�kel�seket f�zhetnek egyes c�mekhez.

A C# console app fel�let�n a k�vetkez� funkci�k el�rhet�ek:
-Filmek list�z�sa/param�terek m�dos�t�sa/t�rl�se
-Felhaszn�l�k list�z�sa/egy�ni list�inak m�dos�t�sa/t�rl�se
-K�l�nb�z� szempontok szerinti csoportos�t�sok (pl.: �rt�kel�sek intervallumok szerint, zs�nerek szerint stb...)
-Statisztik�k k�sz�t�se (filmek megtekint�se, kedvencekhez ad�sok sz�ma)
-A k�l�nb�z� lek�rdez�sek kombin�ci�jak�nt: demogr�fiai kimutat�sok el��ll�t�sa: pl adott zs�nerek mely koroszt�ly, 
vagy nem sz�m�ra n�pszer�bbek

JAVA endpoint:
-megkap egy usert a C# programt�l.
-olvassa a kapott user list�it
-minden egyes list�hoz el��ll�t egy egyedi ID-t (valamilyen HASHel�s seg�ts�g�vel)
-visszaadja egy xml-ben a user list�it, majd ahhoz tartoz� ID-kat
