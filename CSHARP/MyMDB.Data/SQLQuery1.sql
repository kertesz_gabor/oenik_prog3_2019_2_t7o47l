﻿
CREATE TABLE MOVIES 
    (
	 MOVIEID 			 NUMERIC(2) NOT NULL,
	 TITLE           	 VARCHAR(60),
	 LANGUAGE 			 VARCHAR(14),
	 RELEASED			 DATE,
	 RATING				 DECIMAL(2,1),
	 GENRE				 VARCHAR(25),
	 DIRECTOR			 VARCHAR(25),
	 AGE_LIMIT			 NUMERIC(2),
	 WRITERS			 VARCHAR(60),

  CONSTRAINT MOVIES_PK PRIMARY KEY (MOVIEID));

INSERT INTO MOVIES VALUES (1,'The Shawshank Redemption','English',
'1995.05.25',9.3,'Drama','Frank Darabont',16,'Stephen King, Frank Darabont');
INSERT INTO MOVIES VALUES (2,'Pulp Fiction','English',
'1995.04.13',9.0,'Crime, Drama','Quentin Tarantino',18,'Quentin Tarantino, Roger Avary');
INSERT INTO MOVIES VALUES (3,'Se7en','English',
'1996.11.7',8.2,'Crime, Drama, Mystery','David Fincher',18,'Andrew Kevin Walker');
INSERT INTO MOVIES VALUES (4,'Fight Club','English',
'2000.01.27',8.8,'Drama','David Fincher',18,'Chuck Palahniuk, Jim Uhls');
INSERT INTO MOVIES VALUES (5,'Forrest Gump','English',
'1994.12.08',9.5,'Drama, Romance','Robert Zemeckis',12,'Winston Groom, Eric Roth');
INSERT INTO MOVIES VALUES (6,'Inception','English',
'2010.07.22',8.1,'Action, Adventure, Sci-Fi','Christopher Nolan',16,'Christopher Nolan');
INSERT INTO MOVIES VALUES (7,'Goodfellas','English',
'1991.02.14',8.3,'Biography, Crime, Drama','Martin Scorsese',16,'Nicholas Pileggi');
INSERT INTO MOVIES VALUES (8,'Saving Private Ryan','English',
'1998.09.17',8.6,'Drama, War','Steven Spielberg',16,'Robert Rodat');
INSERT INTO MOVIES VALUES (9,'The Green Mile','English',
'2000.04.27',9.1,'Crime, Drama, Fantasy','Frank Darabont',18,'Stephen King, Frank Darabont');
INSERT INTO MOVIES VALUES (10,'The Departed','English',
'2006.11.09',7.8,'Crime, Drama, Thriller','Martin Scorsese',18,'William Monahan, Alan Mak');
INSERT INTO MOVIES VALUES (11,'Django Unchained','English',
'2013.01.17',8.4,'Drama, Western','Quentin Tarantino',18,'Quentin Tarantino');
INSERT INTO MOVIES VALUES (12,'American History X','English',
'1999.04.15',8.0,'Drama,','Tony Kaye',18,'David McKenna');



CREATE TABLE ACTORS
    (ACTORID             NUMERIC(4) NOT NULL,
     NAME                VARCHAR(25),
     BIRTH_DATE          DATE,
	 OSCAR_NOMINATIONS   NUMERIC(2),
	 OSCAR_WINS			 NUMERIC(2),
	 TOTAL_AWARD_WINS 	 NUMERIC(3),
	 TRADEMARK	  	  	 NVARCHAR(MAX),

  CONSTRAINT ACTOR_PRIMARY_KEY PRIMARY KEY (ACTORID));
 
INSERT INTO ACTORS VALUES (1,'Morgan Freeman','1937.06.01',5,1,61,'Rich yet mellow voice');
INSERT INTO ACTORS VALUES (2,'Tim Robbins','1958.10.16',2,1,36,'Films often reflect his liberal political views');
INSERT INTO ACTORS VALUES (3,'John Travolta','1954.02.18',2,0,52,' Dark hair with widow\s peak');
INSERT INTO ACTORS VALUES (4,'Samuel L. Jackson','1948.12.21',1,0,38,'Often plays hotheaded characters with a fiery temper');
INSERT INTO ACTORS VALUES (5,'Tim Roth','1961.05.14',1,0,19,'Often works with director Quentin Tarantino');
INSERT INTO ACTORS VALUES (6,'Brad Pitt','1963.12.18',5,1,70,'Frequently works with David Fincher');
INSERT INTO ACTORS VALUES (7,'Edward Norton','1969.08.18',3,0,46,'Distinctive voice');
INSERT INTO ACTORS VALUES (8,'Tom Hanks','1956.07.09',5,2,83,'Frequently plays ordinary characters in extraordinary situations');
INSERT INTO ACTORS VALUES (9,'Leonardo DiCaprio','1974.11.11',5,1,99,'Often appears in the films of Martin Scorsese');
INSERT INTO ACTORS VALUES (10,'Tom Hardy','1977.09.15',1,0,17,'Frequently works with director Christopher Nolan');
INSERT INTO ACTORS VALUES (11,'Ellen Page','1987.02.21',1,0,43,'Often plays intelligent likeable characters');
INSERT INTO ACTORS VALUES (12,'Robert De Niro','1943.08.17',7,2,56,'Distinctive New York accent');
INSERT INTO ACTORS VALUES (13,'Ray Liotta','1954.12.18',0,0,8,'Piercing blue eyes');
INSERT INTO ACTORS VALUES (14,'Joe Pesci','1943.02.09',2,1,14,'High-pitched voice with New Jersey accent and rapid-fire delivery');
INSERT INTO ACTORS VALUES (15,'Vin Diesel','1937.06.01',5,1,8,'Muscular physique');
INSERT INTO ACTORS VALUES (16,'Michael Clarke Duncan','1957.12.10',1,0,4,'Often plays gentle giants');
INSERT INTO ACTORS VALUES (17,'Jack Nicholson','1937.04.22',12,3,88,'Frequently works as a character with mental instability');
INSERT INTO ACTORS VALUES (18,'Jamie Foxx','1967.12.13',2,1,47,'');

  
  
  
  
  CREATE TABLE ACTORS_IN_MOVIES
	(
		ACTORID  NUMERIC(4)  NOT NULL,
		MOVIEID  NUMERIC(2) NOT NULL,
		
		CONSTRAINT PK PRIMARY KEY (ACTORID,MOVIEID),
		CONSTRAINT FK_ACTOR FOREIGN KEY (ACTORID) REFERENCES ACTORS(ACTORID) ON DELETE CASCADE,
		CONSTRAINT FK_MOVIE FOREIGN KEY (MOVIEID) REFERENCES MOVIES(MOVIEID) ON DELETE CASCADE
	);
	INSERT INTO ACTORS_IN_MOVIES VALUES (1,1);
	INSERT INTO ACTORS_IN_MOVIES VALUES (1,3);
	INSERT INTO ACTORS_IN_MOVIES VALUES (2,1);
	INSERT INTO ACTORS_IN_MOVIES VALUES (3,2);
	INSERT INTO ACTORS_IN_MOVIES VALUES (4,2);
    INSERT INTO ACTORS_IN_MOVIES VALUES (4,11);
	INSERT INTO ACTORS_IN_MOVIES VALUES (5,2);
	INSERT INTO ACTORS_IN_MOVIES VALUES (6,3);
	INSERT INTO ACTORS_IN_MOVIES VALUES (6,4);
	INSERT INTO ACTORS_IN_MOVIES VALUES (6,10);
	INSERT INTO ACTORS_IN_MOVIES VALUES (7,4);
	INSERT INTO ACTORS_IN_MOVIES VALUES (7,12);
	INSERT INTO ACTORS_IN_MOVIES VALUES (9,6);
	INSERT INTO ACTORS_IN_MOVIES VALUES (9,10);
	INSERT INTO ACTORS_IN_MOVIES VALUES (9,11);
	INSERT INTO ACTORS_IN_MOVIES VALUES (8,5);
	INSERT INTO ACTORS_IN_MOVIES VALUES (8,8);
	INSERT INTO ACTORS_IN_MOVIES VALUES (8,9);
	INSERT INTO ACTORS_IN_MOVIES VALUES (10,6);
	INSERT INTO ACTORS_IN_MOVIES VALUES (11,6);
	INSERT INTO ACTORS_IN_MOVIES VALUES (12,7);
	INSERT INTO ACTORS_IN_MOVIES VALUES (13,7);
	INSERT INTO ACTORS_IN_MOVIES VALUES (14,7);
	INSERT INTO ACTORS_IN_MOVIES VALUES (15,8);
	INSERT INTO ACTORS_IN_MOVIES VALUES (16,9);
	INSERT INTO ACTORS_IN_MOVIES VALUES (17,10);
	INSERT INTO ACTORS_IN_MOVIES VALUES (18,11);

	CREATE TABLE USERS
	(
		USERID 					NUMERIC(2) PRIMARY KEY NOT NULL,
		USERNAME				VARCHAR(25),
		EMAIL					VARCHAR(50),
		PASSWORD 				VARCHAR(15),
		NUMBER_OF_REVIEWS		NUMERIC(4),
		AVERAGE_RATING_GIVEN 	DECIMAL(2,1)
	);
	
	INSERT INTO USERS VALUES (1,'John Doe','johndoe@gmail.com','cheese22',1,7.8);
	INSERT INTO USERS VALUES (2,'Edmond Halley','edmond.h@gmail.com','passw0rd',2,8.9);

	CREATE TABLE REVIEWS
	(
		REVIEWID		NUMERIC(2) PRIMARY KEY NOT NULL,
		MOVIEID		    NUMERIC(2) FOREIGN KEY REFERENCES MOVIES(MOVIEID) ON DELETE CASCADE NOT NULL,
		USERID		    NUMERIC(2) FOREIGN KEY REFERENCES USERS(USERID) ON DELETE CASCADE NOT NULL,
		RATING			DECIMAL(2,1),
		REVIEW_TEXT		NVARCHAR(MAX)
	);
	
	INSERT INTO REVIEWS VALUES (1,1,2, 5.7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
	
	CREATE TABLE FAVOURITE_MOVIES
	(
		USERID		NUMERIC(2)  NOT NULL,
		MOVIEID		NUMERIC(2)  NOT NULL,
		
		CONSTRAINT PK PRIMARY KEY (USERID,MOVIEID),
		CONSTRAINT FK_MOVIE FOREIGN KEY (MOVIEID) REFERENCES MOVIES(MOVIEID) ON DELETE CASCADE,
		CONSTRAINT FK_USER  FOREIGN KEY (USERID) REFERENCES USERS(USERID) ON DELETE CASCADE 
	);
	
	INSERT INTO FAVOURITE_MOVIES VALUES(1,5);
	INSERT INTO FAVOURITE_MOVIES VALUES(1,10);
	INSERT INTO FAVOURITE_MOVIES VALUES(1,7);
	INSERT INTO FAVOURITE_MOVIES VALUES(2,1);
	INSERT INTO FAVOURITE_MOVIES VALUES(2,12);
