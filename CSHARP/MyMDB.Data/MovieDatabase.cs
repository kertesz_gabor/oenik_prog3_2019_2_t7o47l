﻿// <copyright file="MovieDatabase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Initalizes a database entity, thus making it passable to higher design levels.
    /// </summary>
    public static class MovieDatabase
    {
        static MovieDatabase()
        {
            MovieDb = new MyMDB_databaseEntities();
        }

        /// <summary>
        /// Gets the database thus making it possible for higher design levels to access it.
        /// </summary>
        public static MyMDB_databaseEntities MovieDb { get; }
    }
}
