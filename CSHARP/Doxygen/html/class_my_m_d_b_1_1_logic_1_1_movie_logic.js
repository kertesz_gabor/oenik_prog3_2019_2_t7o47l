var class_my_m_d_b_1_1_logic_1_1_movie_logic =
[
    [ "MovieLogic", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a9b59d97b879d9cf6ab2d320f7d54d9a5", null ],
    [ "CreateEntity", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a8a495b733c3fa0606600685a98007f7b", null ],
    [ "DeleteEntity", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a88811e066b1fa61fbd6a88e17b1ee701", null ],
    [ "GetAllEntities", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a3e454610f32ffffd8bb19a711e23babe", null ],
    [ "GetAverageRatingPerGenre", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a812eb282f0892451c37e57ea5bea1196", null ],
    [ "GetEntityById", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a6e77aab00373921b7f10872971c1e4a6", null ],
    [ "GetMoviesInWhichAllActorsPlay", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a2181297cc42785e0c5cd38a7042261c7", null ],
    [ "GetTop5Movies", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a2b1b7e56ab663339913997ff058beb8e", null ],
    [ "UpdateEntity", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a3b29268ca5e7f3f18366e56d979e9f06", null ],
    [ "MovieRepository", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a2d53d993b1dad23fff04255cf8c4681a", null ]
];