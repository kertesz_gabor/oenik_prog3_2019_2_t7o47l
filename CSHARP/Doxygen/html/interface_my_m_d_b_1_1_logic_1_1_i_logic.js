var interface_my_m_d_b_1_1_logic_1_1_i_logic =
[
    [ "CreateEntity", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html#aee030f1a447ffef990da3961d5f471f6", null ],
    [ "DeleteEntity", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html#a4e0bd97bcf5c95985f5b7375e5557981", null ],
    [ "GetAllEntities", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html#a9d2788656d425f5ececfd5e3e8b70fa8", null ],
    [ "GetEntityById", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html#ab1b99bb7c43809901d24406c9fd8b14e", null ],
    [ "UpdateEntity", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html#a08242ef5ac3ddb8698a5aa4826d7cac8", null ]
];