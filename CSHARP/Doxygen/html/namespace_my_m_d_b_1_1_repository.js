var namespace_my_m_d_b_1_1_repository =
[
    [ "ActorRepository", "class_my_m_d_b_1_1_repository_1_1_actor_repository.html", "class_my_m_d_b_1_1_repository_1_1_actor_repository" ],
    [ "IRepository", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html", "interface_my_m_d_b_1_1_repository_1_1_i_repository" ],
    [ "MovieRepository", "class_my_m_d_b_1_1_repository_1_1_movie_repository.html", "class_my_m_d_b_1_1_repository_1_1_movie_repository" ],
    [ "ReviewRepository", "class_my_m_d_b_1_1_repository_1_1_review_repository.html", "class_my_m_d_b_1_1_repository_1_1_review_repository" ],
    [ "UserRepository", "class_my_m_d_b_1_1_repository_1_1_user_repository.html", "class_my_m_d_b_1_1_repository_1_1_user_repository" ]
];