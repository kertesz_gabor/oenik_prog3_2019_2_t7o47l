var class_my_m_d_b_1_1_logic_1_1_review_logic =
[
    [ "ReviewLogic", "class_my_m_d_b_1_1_logic_1_1_review_logic.html#a25d67b72b58baf22ea818389797f527b", null ],
    [ "CreateEntity", "class_my_m_d_b_1_1_logic_1_1_review_logic.html#a09ccd002f539de898c726d7a39f60d7b", null ],
    [ "DeleteEntity", "class_my_m_d_b_1_1_logic_1_1_review_logic.html#a13b00310834ff68a952a134806932de5", null ],
    [ "GetAllEntities", "class_my_m_d_b_1_1_logic_1_1_review_logic.html#a3c34dd577823df7c7a3df377bf22b494", null ],
    [ "GetEntityById", "class_my_m_d_b_1_1_logic_1_1_review_logic.html#a98a93f9283717fddb533f98e2c86cb91", null ],
    [ "UpdateEntity", "class_my_m_d_b_1_1_logic_1_1_review_logic.html#a5df916387254720b90510f1a22b5aca8", null ],
    [ "ReviewRepository", "class_my_m_d_b_1_1_logic_1_1_review_logic.html#aa7d47c0e14af624953c2fd8853318f17", null ]
];