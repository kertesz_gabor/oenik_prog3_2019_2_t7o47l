var namespace_my_m_d_b_1_1_logic =
[
    [ "Tests", "namespace_my_m_d_b_1_1_logic_1_1_tests.html", "namespace_my_m_d_b_1_1_logic_1_1_tests" ],
    [ "ActorLogic", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html", "class_my_m_d_b_1_1_logic_1_1_actor_logic" ],
    [ "IActorLogic", "interface_my_m_d_b_1_1_logic_1_1_i_actor_logic.html", "interface_my_m_d_b_1_1_logic_1_1_i_actor_logic" ],
    [ "ILogic", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html", "interface_my_m_d_b_1_1_logic_1_1_i_logic" ],
    [ "IMovieLogic", "interface_my_m_d_b_1_1_logic_1_1_i_movie_logic.html", "interface_my_m_d_b_1_1_logic_1_1_i_movie_logic" ],
    [ "IReviewLogic", "interface_my_m_d_b_1_1_logic_1_1_i_review_logic.html", "interface_my_m_d_b_1_1_logic_1_1_i_review_logic" ],
    [ "IUserLogic", "interface_my_m_d_b_1_1_logic_1_1_i_user_logic.html", "interface_my_m_d_b_1_1_logic_1_1_i_user_logic" ],
    [ "MovieLogic", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html", "class_my_m_d_b_1_1_logic_1_1_movie_logic" ],
    [ "ReviewLogic", "class_my_m_d_b_1_1_logic_1_1_review_logic.html", "class_my_m_d_b_1_1_logic_1_1_review_logic" ],
    [ "UserLogic", "class_my_m_d_b_1_1_logic_1_1_user_logic.html", "class_my_m_d_b_1_1_logic_1_1_user_logic" ]
];