var searchData=
[
  ['data_38',['Data',['../namespace_my_m_d_b_1_1_data.html',1,'MyMDB']]],
  ['logic_39',['Logic',['../namespace_my_m_d_b_1_1_logic.html',1,'MyMDB']]],
  ['main_40',['Main',['../class_program_1_1_program.html#a920403aa97708c8980397aaa902a9b02',1,'Program::Program']]],
  ['microsoft_41',['Microsoft',['../md__c_1__users__kert_xC3_xA9sz__g_xC3_xA1bor_source_repos_prog3_feleves__c_s_h_a_r_p_packages__m063b70c5f38c4bc388eeb68e9bd2ed51.html',1,'']]],
  ['movie_42',['MOVIE',['../class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html',1,'MyMDB::Data']]],
  ['movielogic_43',['MovieLogic',['../class_my_m_d_b_1_1_logic_1_1_movie_logic.html',1,'MyMDB.Logic.MovieLogic'],['../class_my_m_d_b_1_1_logic_1_1_tests_1_1_test.html#a5e4659db8cad5d3a775de4fc51e20654',1,'MyMDB.Logic.Tests.Test.MovieLogic()'],['../class_program_1_1_program_1_1_program_instance.html#aae5d9cbdf6504e0b77821b3eb64962a7',1,'Program.Program.ProgramInstance.MovieLogic()'],['../class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a9b59d97b879d9cf6ab2d320f7d54d9a5',1,'MyMDB.Logic.MovieLogic.MovieLogic()']]],
  ['movierepository_44',['MovieRepository',['../class_my_m_d_b_1_1_repository_1_1_movie_repository.html',1,'MyMDB.Repository.MovieRepository'],['../interface_my_m_d_b_1_1_logic_1_1_i_movie_logic.html#aa31ec0933fa6e60b5226a88c5caba49d',1,'MyMDB.Logic.IMovieLogic.MovieRepository()'],['../class_my_m_d_b_1_1_logic_1_1_movie_logic.html#a2d53d993b1dad23fff04255cf8c4681a',1,'MyMDB.Logic.MovieLogic.MovieRepository()'],['../class_my_m_d_b_1_1_repository_1_1_movie_repository.html#ad975a90fa548e0cff5c518a07af4b622',1,'MyMDB.Repository.MovieRepository.MovieRepository()']]],
  ['movietester_45',['MovieTester',['../class_my_m_d_b_1_1_logic_1_1_tests_1_1_movie_tester.html',1,'MyMDB::Logic::Tests']]],
  ['mymdb_46',['MyMDB',['../namespace_my_m_d_b.html',1,'']]],
  ['mymdb_5fdatabaseentities_47',['MyMDB_databaseEntities',['../class_my_m_d_b_1_1_data_1_1_my_m_d_b__database_entities.html',1,'MyMDB::Data']]],
  ['repository_48',['Repository',['../namespace_my_m_d_b_1_1_repository.html',1,'MyMDB']]],
  ['tests_49',['Tests',['../namespace_my_m_d_b_1_1_logic_1_1_tests.html',1,'MyMDB::Logic']]]
];
