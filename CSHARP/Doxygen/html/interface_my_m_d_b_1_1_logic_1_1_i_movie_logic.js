var interface_my_m_d_b_1_1_logic_1_1_i_movie_logic =
[
    [ "GetAverageRatingPerGenre", "interface_my_m_d_b_1_1_logic_1_1_i_movie_logic.html#ab20f0daa1e81678678a0b712590ddc59", null ],
    [ "GetMoviesInWhichAllActorsPlay", "interface_my_m_d_b_1_1_logic_1_1_i_movie_logic.html#af0e8dba1357d0a07eed2ebaf81e980c1", null ],
    [ "GetTop5Movies", "interface_my_m_d_b_1_1_logic_1_1_i_movie_logic.html#a032f4550aae6be6ab310082d2bf324a2", null ],
    [ "MovieRepository", "interface_my_m_d_b_1_1_logic_1_1_i_movie_logic.html#aa31ec0933fa6e60b5226a88c5caba49d", null ]
];