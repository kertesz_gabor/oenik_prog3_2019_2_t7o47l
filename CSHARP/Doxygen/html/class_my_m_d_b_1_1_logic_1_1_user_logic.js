var class_my_m_d_b_1_1_logic_1_1_user_logic =
[
    [ "UserLogic", "class_my_m_d_b_1_1_logic_1_1_user_logic.html#aea83b462c0f31e6d37a95957ba338089", null ],
    [ "CreateEntity", "class_my_m_d_b_1_1_logic_1_1_user_logic.html#a5c78e5a624b12d180ac9402ded30d574", null ],
    [ "DeleteEntity", "class_my_m_d_b_1_1_logic_1_1_user_logic.html#ac3e53a8afd2b2f33eca66bdc6c5d44cb", null ],
    [ "GetAllEntities", "class_my_m_d_b_1_1_logic_1_1_user_logic.html#a2b588fb49402b5b2982feb544ed7029f", null ],
    [ "GetEntityById", "class_my_m_d_b_1_1_logic_1_1_user_logic.html#a1a8ca4ea40ea6a0bf7d6e9890efc2502", null ],
    [ "GetUserWithMostReviewsAndHighestRatings", "class_my_m_d_b_1_1_logic_1_1_user_logic.html#a7d878048150bbaf8f5ea44e68cdfb5c4", null ],
    [ "UpdateEntity", "class_my_m_d_b_1_1_logic_1_1_user_logic.html#a5b3e577a1e3010ef642b2f185891806b", null ],
    [ "UserRepository", "class_my_m_d_b_1_1_logic_1_1_user_logic.html#aee9b7dd18da652341013aeb020070746", null ]
];