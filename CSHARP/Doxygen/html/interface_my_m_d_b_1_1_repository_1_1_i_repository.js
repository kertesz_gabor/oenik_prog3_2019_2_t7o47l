var interface_my_m_d_b_1_1_repository_1_1_i_repository =
[
    [ "CreateEntity", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html#a83f646cd3aa89ba388175d8cf31c4803", null ],
    [ "DeleteEntity", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html#a253d0dd2bdc07fa8000708f59d91a1f2", null ],
    [ "GetAllEntities", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html#af5fa6bd8127c062d68c93130646619bb", null ],
    [ "GetEntity", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html#ac65d4493b9f6239010e1da3a26577113", null ],
    [ "GetLastId", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html#abd22a2005d1e2333788e503d89dfdd2e", null ],
    [ "UpdateEntity", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html#a038fa7f9263a74a31a3444361b58a6a1", null ]
];