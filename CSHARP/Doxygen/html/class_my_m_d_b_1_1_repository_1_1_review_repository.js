var class_my_m_d_b_1_1_repository_1_1_review_repository =
[
    [ "ReviewRepository", "class_my_m_d_b_1_1_repository_1_1_review_repository.html#a02db6491d5fe542031eb7222003e4811", null ],
    [ "CreateEntity", "class_my_m_d_b_1_1_repository_1_1_review_repository.html#a77bb92cfdf8448b7bbb57bdfc9afe390", null ],
    [ "DeleteEntity", "class_my_m_d_b_1_1_repository_1_1_review_repository.html#a13dbdde33b204210840dca1839edabd8", null ],
    [ "GetAllEntities", "class_my_m_d_b_1_1_repository_1_1_review_repository.html#a1bdc3503c9c452cc7654c744338de336", null ],
    [ "GetEntity", "class_my_m_d_b_1_1_repository_1_1_review_repository.html#addbf291e450cb987e0feba04cb3605b5", null ],
    [ "GetLastId", "class_my_m_d_b_1_1_repository_1_1_review_repository.html#acbeb197d57f84d070266753c7df179dd", null ],
    [ "UpdateEntity", "class_my_m_d_b_1_1_repository_1_1_review_repository.html#a888735dea57647e5447476b470eff5e3", null ]
];