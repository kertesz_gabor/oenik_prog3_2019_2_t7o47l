var class_my_m_d_b_1_1_data_1_1_u_s_e_r =
[
    [ "USER", "class_my_m_d_b_1_1_data_1_1_u_s_e_r.html#a4d1f394f9535729fac7f2a2d4ddae50b", null ],
    [ "AVERAGE_RATING_GIVEN", "class_my_m_d_b_1_1_data_1_1_u_s_e_r.html#a9972a471039a3b478bf1f9d3236e75be", null ],
    [ "EMAIL", "class_my_m_d_b_1_1_data_1_1_u_s_e_r.html#a8cfa040f3b0e9fee847e172e77674527", null ],
    [ "NUMBER_OF_REVIEWS", "class_my_m_d_b_1_1_data_1_1_u_s_e_r.html#a952cc1cd5eed653c41ac26c6cc8cbc56", null ],
    [ "PASSWORD", "class_my_m_d_b_1_1_data_1_1_u_s_e_r.html#a075ae4cc10c848b978d683b123a84540", null ],
    [ "REVIEWS", "class_my_m_d_b_1_1_data_1_1_u_s_e_r.html#adb8328dedcb44e810e15de6f7b218c8d", null ],
    [ "USERID", "class_my_m_d_b_1_1_data_1_1_u_s_e_r.html#a55830ba046d442448eee53e18b4eca72", null ],
    [ "USERNAME", "class_my_m_d_b_1_1_data_1_1_u_s_e_r.html#a4be8b9639cfffc21469abf62c18332c6", null ]
];