var class_my_m_d_b_1_1_logic_1_1_actor_logic =
[
    [ "ActorLogic", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html#a91a82ecc07d35f3aee569a6b51d155f6", null ],
    [ "CreateEntity", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html#aa1bdbeda0000ea92b39e6f594b1c3c6c", null ],
    [ "DeleteEntity", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html#a6082d5085d7017ea0052859f443156ba", null ],
    [ "GetAllActorsFavDirectors", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html#aef8b7b2b1e9e1ef1a534418c8a765048", null ],
    [ "GetAllEntities", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html#abaf5648083677246323885c64f93ff21", null ],
    [ "GetEntityById", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html#ac444a7e0d859905cd8369b3c3cdbe6b3", null ],
    [ "UpdateEntity", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html#a956a5e198173425720252db56c6c2453", null ],
    [ "ActorRepository", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html#a736b8df0d5337821d28b43e15c1a768c", null ]
];