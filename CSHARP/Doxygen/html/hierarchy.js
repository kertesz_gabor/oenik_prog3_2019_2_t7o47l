var hierarchy =
[
    [ "MyMDB.Data.ACTOR", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html", null ],
    [ "DbContext", null, [
      [ "MyMDB.Data::MyMDB_databaseEntities", "class_my_m_d_b_1_1_data_1_1_my_m_d_b__database_entities.html", null ]
    ] ],
    [ "MyMDB.Logic.ILogic< T >", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html", null ],
    [ "MyMDB.Logic.ILogic< ACTOR >", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html", [
      [ "MyMDB.Logic.IActorLogic", "interface_my_m_d_b_1_1_logic_1_1_i_actor_logic.html", [
        [ "MyMDB.Logic.ActorLogic", "class_my_m_d_b_1_1_logic_1_1_actor_logic.html", null ]
      ] ]
    ] ],
    [ "MyMDB.Logic.ILogic< MOVIE >", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html", [
      [ "MyMDB.Logic.IMovieLogic", "interface_my_m_d_b_1_1_logic_1_1_i_movie_logic.html", [
        [ "MyMDB.Logic.MovieLogic", "class_my_m_d_b_1_1_logic_1_1_movie_logic.html", null ]
      ] ]
    ] ],
    [ "MyMDB.Logic.ILogic< REVIEW >", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html", [
      [ "MyMDB.Logic.IReviewLogic", "interface_my_m_d_b_1_1_logic_1_1_i_review_logic.html", [
        [ "MyMDB.Logic.ReviewLogic", "class_my_m_d_b_1_1_logic_1_1_review_logic.html", null ]
      ] ]
    ] ],
    [ "MyMDB.Logic.ILogic< USER >", "interface_my_m_d_b_1_1_logic_1_1_i_logic.html", [
      [ "MyMDB.Logic.IUserLogic", "interface_my_m_d_b_1_1_logic_1_1_i_user_logic.html", [
        [ "MyMDB.Logic.UserLogic", "class_my_m_d_b_1_1_logic_1_1_user_logic.html", null ]
      ] ]
    ] ],
    [ "MyMDB.Repository.IRepository< T >", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html", null ],
    [ "MyMDB.Repository.IRepository< ACTOR >", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html", [
      [ "MyMDB.Repository.ActorRepository", "class_my_m_d_b_1_1_repository_1_1_actor_repository.html", null ]
    ] ],
    [ "MyMDB.Repository.IRepository< MOVIE >", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html", [
      [ "MyMDB.Repository.MovieRepository", "class_my_m_d_b_1_1_repository_1_1_movie_repository.html", null ]
    ] ],
    [ "MyMDB.Repository.IRepository< REVIEW >", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html", [
      [ "MyMDB.Repository.ReviewRepository", "class_my_m_d_b_1_1_repository_1_1_review_repository.html", null ]
    ] ],
    [ "MyMDB.Repository.IRepository< USER >", "interface_my_m_d_b_1_1_repository_1_1_i_repository.html", [
      [ "MyMDB.Repository.UserRepository", "class_my_m_d_b_1_1_repository_1_1_user_repository.html", null ]
    ] ],
    [ "MyMDB.Data.MOVIE", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html", null ],
    [ "Program.Program", "class_program_1_1_program.html", null ],
    [ "Program.Program.ProgramInstance", "class_program_1_1_program_1_1_program_instance.html", null ],
    [ "MyMDB.Data.REVIEW", "class_my_m_d_b_1_1_data_1_1_r_e_v_i_e_w.html", null ],
    [ "MyMDB.Logic.Tests.Test", "class_my_m_d_b_1_1_logic_1_1_tests_1_1_test.html", [
      [ "MyMDB.Logic.Tests.ActorTester", "class_my_m_d_b_1_1_logic_1_1_tests_1_1_actor_tester.html", null ],
      [ "MyMDB.Logic.Tests.MovieTester", "class_my_m_d_b_1_1_logic_1_1_tests_1_1_movie_tester.html", null ],
      [ "MyMDB.Logic.Tests.UserTester", "class_my_m_d_b_1_1_logic_1_1_tests_1_1_user_tester.html", null ]
    ] ],
    [ "MyMDB.Data.USER", "class_my_m_d_b_1_1_data_1_1_u_s_e_r.html", null ]
];