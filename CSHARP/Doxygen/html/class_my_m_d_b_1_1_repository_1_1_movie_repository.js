var class_my_m_d_b_1_1_repository_1_1_movie_repository =
[
    [ "MovieRepository", "class_my_m_d_b_1_1_repository_1_1_movie_repository.html#ad975a90fa548e0cff5c518a07af4b622", null ],
    [ "CreateEntity", "class_my_m_d_b_1_1_repository_1_1_movie_repository.html#a697042269e1a3548721b4c5cc655c8f9", null ],
    [ "DeleteEntity", "class_my_m_d_b_1_1_repository_1_1_movie_repository.html#a222ef1cf2be6e7abcea0d8c5ed78e5ff", null ],
    [ "GetAllEntities", "class_my_m_d_b_1_1_repository_1_1_movie_repository.html#afc0f2c763481bfca8c18e86ff62d6078", null ],
    [ "GetEntity", "class_my_m_d_b_1_1_repository_1_1_movie_repository.html#a17133a8f17e8e0da101e828d274ddf92", null ],
    [ "GetLastId", "class_my_m_d_b_1_1_repository_1_1_movie_repository.html#a0e0e494c295b02750719486352eeea9b", null ],
    [ "UpdateEntity", "class_my_m_d_b_1_1_repository_1_1_movie_repository.html#ae79268dcc26abebe2be8315581aaeb0c", null ]
];