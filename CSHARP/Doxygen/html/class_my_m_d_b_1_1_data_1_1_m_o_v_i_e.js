var class_my_m_d_b_1_1_data_1_1_m_o_v_i_e =
[
    [ "MOVIE", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#a98fda11e39f21790ec417b1c7d20deab", null ],
    [ "ACTORS", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#aa2c96f6bd5ea0a3e3ae5f34066096ef7", null ],
    [ "AGE_LIMIT", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#addfdddd8fd3bc64dfb109777aea56a2a", null ],
    [ "DIRECTOR", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#afe8a8ee26b575878c6d52c2360a2aaa8", null ],
    [ "GENRE", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#a3270cfd36ad815b2d8e92eabdbe71728", null ],
    [ "LANGUAGE", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#a7f4f9d3f3b102032e282d79ba2e08a1a", null ],
    [ "MOVIEID", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#ae8c93c26b945b3fa1b06073baaa06ec5", null ],
    [ "RATING", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#a49c7e99c3a249d89feb47385fae0bb30", null ],
    [ "RELEASED", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#a786d01aa4f81e1071fe0d3ae99f93898", null ],
    [ "REVIEWS", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#a3de63bf13d011fd0a63f1d322a3df2eb", null ],
    [ "TITLE", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#ab4837cd8adf255589d5cbf0af43066c3", null ],
    [ "WRITERS", "class_my_m_d_b_1_1_data_1_1_m_o_v_i_e.html#a20ccc2625e08bbaa9bdcf1dadf7d641b", null ]
];