var class_my_m_d_b_1_1_data_1_1_a_c_t_o_r =
[
    [ "ACTOR", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html#a54809aa6306e9f9de4e6e7be09f40ef1", null ],
    [ "ACTORID", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html#a112c2acf1768a362c8045a78acbe1b2f", null ],
    [ "BIRTH_DATE", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html#a0ba47b7caca69d73e9a32cc3f9f5fb21", null ],
    [ "MOVIES", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html#a3a924da772fa4039b8d89fd05567083c", null ],
    [ "NAME", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html#a051b3129debb11c444fa64dd6c3775d8", null ],
    [ "OSCAR_NOMINATIONS", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html#ac615cf5c88818736ef95a8ed7c5a6bca", null ],
    [ "OSCAR_WINS", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html#a3310e90df46e6ecb5b902986c2214c19", null ],
    [ "TOTAL_AWARD_WINS", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html#abfedb598c3b8cc1aa8b5fd260d52df55", null ],
    [ "TRADEMARK", "class_my_m_d_b_1_1_data_1_1_a_c_t_o_r.html#acaac8be2bb99e65505936046a32d35c2", null ]
];