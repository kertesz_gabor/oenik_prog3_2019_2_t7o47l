﻿// <copyright file="IReviewLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyMDB.Data;
    using MyMDB.Repository;

    /// <summary>
    /// Defines the business logic, including entity-specific abilities of a logic class working with reviews.
    /// </summary>
    public interface IReviewLogic : ILogic<REVIEW>
    {
        /// <summary>
        /// Gets or sets an <see cref="ReviewRepository"/> object that communicates with the database.
        /// </summary>
        IRepository<REVIEW> ReviewRepository { get; set; }
    }

    /// <summary>
    /// Defines the business logic revolving around the reviews of the database needed to satisfy the demands of the frontend of the application.
    /// Also contains the logic needed for basic CRUD methods.
    /// </summary>
    public class ReviewLogic : IReviewLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReviewLogic"/> class.
        /// </summary>
        public ReviewLogic()
        {
            this.ReviewRepository = new ReviewRepository();
        }

        /// <summary>
        /// Gets or sets an <see cref="ReviewRepository"/> object that communicates with the database.
        /// </summary>
        public IRepository<REVIEW> ReviewRepository { get; set; }

        /// <summary>
        /// Passes a review instance onto the <see cref="ReviewRepository"/> in order to have it created in the database.
        /// </summary>
        /// <param name="review">A new review to add.</param>
        public void CreateEntity(REVIEW review)
        {
            review.REVIEWID = this.ReviewRepository.GetLastId() + 1;
            this.ReviewRepository.CreateEntity(review);
        }

        /// <summary>
        /// Deletes a review instance.
        /// </summary>
        /// <param name="id">The id of the review to delete.</param>
        public void DeleteEntity(int id)
        {
            this.ReviewRepository.DeleteEntity(id);
        }

        /// <summary>
        /// Updates a review of a given id.
        /// </summary>
        /// <param name="oldReviewsId">The id of the review.</param>
        /// <param name="review">The review object.</param>
        public void UpdateEntity(int oldReviewsId, REVIEW review)
        {
            this.ReviewRepository.UpdateEntity(oldReviewsId, review);
        }

        /// <summary>
        /// Returns a Review object of the specified id.
        /// </summary>
        /// <param name="id">The id of the desired review.</param>
        /// <returns>REVIEW.</returns>
        public REVIEW GetEntityById(int id)
        {
            return this.ReviewRepository.GetEntity(id);
        }

        /// <summary>
        /// Returns all Review elements found in the database.
        /// </summary>
        /// <returns>An reviews collection.</returns>
        public IList<REVIEW> GetAllEntities()
        {
            return this.ReviewRepository.GetAllEntities().ToList();
        }
    }
}
