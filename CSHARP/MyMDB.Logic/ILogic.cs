﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides basic CRUD abilites for implementing logic classes.
    /// Should be extended by entity-specific subinterfaces.
    /// </summary>
    /// <typeparam name="T">Any entity type.</typeparam>
    public interface ILogic<T>
    {
        /// <summary>
        /// Passes an entity to the working repository in order to have it created in the database.
        /// </summary>
        /// <param name="entityToAdd">The desired entity to add.</param>
        void CreateEntity(T entityToAdd);

        /// <summary>
        /// Gets an entity from the database.
        /// </summary>
        /// <param name="id">The desired entity's id.</param>
        /// <returns>Any entity tpye.</returns>
        T GetEntityById(int id);

        /// <summary>
        /// Updates an entity in the database.
        /// </summary>
        /// <param name="oldId">The old id of the entity to update.</param>
        /// <param name="entityToUpdate">The desired entity to update.</param>
        void UpdateEntity(int oldId, T entityToUpdate);

        /// <summary>
        /// Deletes an entity from the database.
        /// </summary>
        /// <param name="id">The desired entity's id.</param>
        void DeleteEntity(int id);

        /// <summary>
        /// Gets all entities from the database table.
        /// </summary>
        /// <returns>An indexable list with the working table's entities in it.</returns>
        IList<T> GetAllEntities();
    }
}
