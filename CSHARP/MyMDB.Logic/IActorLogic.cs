﻿// <copyright file="IActorLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyMDB.Data;
    using MyMDB.Repository;

    /// <summary>
    /// Defines the business logic, including entity-specific abilities of a logic class working with actors.
    /// </summary>
    public interface IActorLogic : ILogic<ACTOR>
    {
        /// <summary>
        /// Gets or sets an <see cref="ActorRepository"/> object that communicates with the database.
        /// </summary>
        IRepository<ACTOR> ActorRepository { get; set; }

        /// <summary>
        /// For each actor, returns the director they worked with the most.
        /// </summary>
        /// <returns>A list of string where lines are indented by tabs.</returns>
        IList<string> GetAllActorsFavDirectors();
    }

    /// <summary>
    /// Defines the business logic revolving around the actors of the database needed to satisfy the demands of the frontend of the application.
    /// Also contains the logic needed for basic CRUD methods.
    /// </summary>
    public class ActorLogic : IActorLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActorLogic"/> class.
        /// </summary>
        public ActorLogic()
        {
            this.ActorRepository = new ActorRepository();
        }

        /// <summary>
        /// Gets or sets an <see cref="ActorRepository"/> object that communicates with the database.
        /// </summary>
        public IRepository<ACTOR> ActorRepository { get; set; }

        /// <summary>
        /// Passes an actor instance onto the <see cref="ActorRepository"/> in order to have it created in the database.
        /// </summary>
        /// <param name="actor">A new actor to add.</param>
        public void CreateEntity(ACTOR actor)
        {
            actor.ACTORID = this.ActorRepository.GetLastId() + 1;
            this.ActorRepository.CreateEntity(actor);
        }

        /// <summary>
        /// Deletes an actor instance.
        /// </summary>
        /// <param name="id">The id of the actor to delete.</param>
        public void DeleteEntity(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("Invalid id was provided.");
            }
            else if (this.ActorRepository.GetAllEntities().ToList().Where(t => t.ACTORID == id).Count() == 0)
            {
                throw new KeyNotFoundException("No entity was found with the specified ID.");
            }
            else
            {
                this.ActorRepository.DeleteEntity(id);
            }
        }

        /// <summary>
        /// Updates an actor of a given id.
        /// </summary>
        /// <param name="oldActorsId">The id of the actor.</param>
        /// <param name="actor">The actor object.</param>
        public void UpdateEntity(int oldActorsId, ACTOR actor)
        {
            if (oldActorsId < 1)
            {
                throw new ArgumentException("Invalid id was provided.");
            }
            else if (this.ActorRepository.GetAllEntities().ToList().Where(t => t.ACTORID == oldActorsId).Count() == 0)
            {
                throw new KeyNotFoundException("No entity was found with the specified ID.");
            }
            else
            {
                this.ActorRepository.UpdateEntity(oldActorsId, actor);
            }
        }

        /// <summary>
        /// Returns a Actor object of the specified id.
        /// </summary>
        /// <param name="id">The id of the desired actor.</param>
        /// <returns>ACTOR.</returns>
        public ACTOR GetEntityById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("Invalid id was provided.");
            }
            else if (this.ActorRepository.GetAllEntities().ToList().Where(t => t.ACTORID == id).Count() == 0)
            {
                throw new KeyNotFoundException("No entity was found with the specified ID.");
            }
            else
            {
                return this.ActorRepository.GetEntity(id);
            }
        }

        /// <summary>
        /// Returns all Actor elements found in the database.
        /// </summary>
        /// <returns>An actors collection.</returns>
        public IList<ACTOR> GetAllEntities()
        {
            return this.ActorRepository.GetAllEntities().ToList();
        }

        /// <summary>
        /// For each actor, returns the director they worked with the most.
        /// </summary>
        /// <returns>A list of string where lines are indented by tabs.</returns>
        public IList<string> GetAllActorsFavDirectors()
        {
            var getResult = from x in this.ActorRepository.GetAllEntities()
                            let favDirector =
                                (from y in x.MOVIES
                                 group y by y.DIRECTOR into g
                                 select new
                                 {
                                     DirectorName = g.Key,
                                     Count = g.Count(),
                                 }).OrderByDescending(t => t.Count).FirstOrDefault()
                            select new
                            {
                                Actor = x.NAME,
                                Fav_director = favDirector.DirectorName,
                            };

            Console.WriteLine(getResult.Count());

            IList<string> results = new List<string>();
            foreach (var item in getResult)
            {
                StringBuilder str = new StringBuilder();
                str.Append($"{item.Actor} legtöbbször {item.Fav_director} rendezővel dolgozott.");
                results.Add(str.ToString());
            }

            return results;
        }
    }
}
