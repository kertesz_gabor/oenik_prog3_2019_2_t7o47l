﻿// <copyright file="IMovieLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyMDB.Data;
    using MyMDB.Repository;

    /// <summary>
    /// Defines the business logic, including entity-specific abilities of a logic class working with movies.
    /// </summary>
    public interface IMovieLogic : ILogic<MOVIE>
    {
        /// <summary>
        /// Gets or sets an <see cref="MovieRepository"/> object that communicates with the database.
        /// </summary>
        IRepository<MOVIE> MovieRepository { get; set; }

        /// <summary>
        /// Returns the value of average user rating for each genre.
        /// </summary>
        /// <returns>An idexable list.</returns>
        IList<string> GetAverageRatingPerGenre();

        /// <summary>
        /// Given a list of actors, returns all the movies which they all appear in.
        /// </summary>
        /// <param name="actors">The list of actors.</param>
        /// <returns>A list of movies containing the titles in which all actors appear.</returns>
        IList<MOVIE> GetMoviesInWhichAllActorsPlay(List<ACTOR> actors);

        /// <summary>
        /// Returns the top 5 highest rated movies in the database.
        /// </summary>
        /// <returns>A list of 5 movies.</returns>
        IList<MOVIE> GetTop5Movies();
    }

    /// <summary>
    /// Defines the business logic needed to satisfy the demands of the frontend of the application.
    /// Also contains the logic needed for basic CRUD methods.
    /// </summary>
    public class MovieLogic : IMovieLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MovieLogic"/> class.
        /// </summary>
        public MovieLogic()
        {
            this.MovieRepository = new MovieRepository();
        }

        /// <summary>
        /// Gets or sets an <see cref="MovieRepository"/> object that communicates with the database.
        /// </summary>
        public IRepository<MOVIE> MovieRepository { get; set; }

        /// <summary>
        /// Passes a movie instance onto the <see cref="MovieRepository"/> in order to have it created in the database.
        /// </summary>
        /// <param name="movie">A new movie to add.</param>
        public void CreateEntity(MOVIE movie)
        {
            movie.MOVIEID = this.MovieRepository.GetLastId() + 1;
            this.MovieRepository.CreateEntity(movie);
        }

        /// <summary>
        /// Deletes a movie instance.
        /// </summary>
        /// <param name="id">The id of the movie to delete.</param>
        public void DeleteEntity(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("Invalid id was provided.");
            }
            else if (this.MovieRepository.GetAllEntities().ToList().Where(t => t.MOVIEID == id).Count() == 0)
            {
                throw new KeyNotFoundException("No entity was found with the specified ID.");
            }
            else
            {
                this.MovieRepository.DeleteEntity(id);
            }
        }

        /// <summary>
        /// Updates a movie of a given id.
        /// </summary>
        /// <param name="oldMoviesId">The id of the movie.</param>
        /// <param name="movie">The movie object.</param>
        public void UpdateEntity(int oldMoviesId, MOVIE movie)
        {
            if (oldMoviesId < 1)
            {
                throw new ArgumentException("Invalid id was provided.");
            }
            else if (this.MovieRepository.GetAllEntities().ToList().Where(t => t.MOVIEID == oldMoviesId).Count() == 0)
            {
                throw new KeyNotFoundException("No entity was found with the specified ID.");
            }
            else
            {
                this.MovieRepository.UpdateEntity(oldMoviesId, movie);
            }
        }

        /// <summary>
        /// Returns a Movie object of the specified id.
        /// </summary>
        /// <param name="id">The id of the desired movie.</param>
        /// <returns>MOVIE.</returns>
        public MOVIE GetEntityById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentException("Invalid id was provided.");
            }
            else if (this.MovieRepository.GetAllEntities().ToList().Where(t => t.MOVIEID == id).Count() == 0)
            {
                throw new KeyNotFoundException("No entity was found with the specified ID.");
            }
            else
            {
                return this.MovieRepository.GetEntity(id);
            }
        }

        /// <summary>
        /// Returns all Movie elements found in the database.
        /// </summary>
        /// <returns>A Movies collection.</returns>
        public IList<MOVIE> GetAllEntities()
        {
            return this.MovieRepository.GetAllEntities().ToList();
        }

        /// <summary>
        /// Returns the top 5 highest rated movies in the database.
        /// </summary>
        /// <returns>A list of 5 movies.</returns>
        public IList<MOVIE> GetTop5Movies()
        {
            return (from x in this.MovieRepository.GetAllEntities()
                    orderby x.RATING descending
                    select x).Take(5).ToList();
        }

        /// <summary>
        /// Given a list of actors, returns all the movies which they all appear in.
        /// </summary>
        /// <param name="actors">The list of actors.</param>
        /// <returns>A list of movies containing the titles in which all actors appear.</returns>
        public IList<MOVIE> GetMoviesInWhichAllActorsPlay(List<ACTOR> actors)
        {
            IList<MOVIE> filteredMovies = new List<MOVIE>();
            foreach (var movie in this.MovieRepository.GetAllEntities())
            {
                var intersection = movie.ACTORS.Intersect(actors);
                if (intersection.Count() == actors.Count())
                {
                    filteredMovies.Add(movie);
                }
            }

            return filteredMovies;
        }

        /// <summary>
        /// Returns the value of average user rating for each genre.
        /// </summary>
        /// <returns>A list of string where lines are indented by tabs.</returns>
        public IList<string> GetAverageRatingPerGenre()
        {
            var getData = from x in this.MovieRepository.GetAllEntities()
                          group x by x.GENRE into g
                          select new
                          {
                              Genre = g.Key,
                              AverageRating = g.Average(t => t.RATING),
                          };

            IList<string> results = new List<string>();
            foreach (var item in getData)
            {
                StringBuilder str = new StringBuilder();
                str.Append(string.Format("A {0} zsánerhez tartozó értékelések átlaga: {1}", item.Genre, item.AverageRating));
                results.Add(str.ToString());
            }

            return results;
        }
    }
}
