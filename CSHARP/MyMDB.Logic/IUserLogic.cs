﻿// <copyright file="IUserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyMDB.Data;
    using MyMDB.Repository;

    /// <summary>
    /// Defines the business logic, including entity-specific abilities of a logic class working with users.
    /// </summary>
    public interface IUserLogic : ILogic<USER>
    {
        /// <summary>
        /// Gets or sets an <see cref="UserRepository"/> object that communicates with the database.
        /// </summary>
        IRepository<USER> UserRepository { get; set; }

        /// <summary>
        /// Returns the user entity with the most number of reviews posted.
        /// </summary>
        /// <returns>A user entity.</returns>
        USER GetUserWithMostReviewsAndHighestRatings();
    }

    /// <summary>
    /// Defines the business logic revolving around the users of the database needed to satisfy the demands of the frontend of the application.
    /// Also contains the logic needed for basic CRUD methods.
    /// </summary>
    public class UserLogic : IUserLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserLogic"/> class.
        /// </summary>
        public UserLogic()
        {
            this.UserRepository = new UserRepository();
        }

        /// <summary>
        /// Gets or sets an <see cref="UserRepository"/> object that communicates with the database.
        /// </summary>
        public IRepository<USER> UserRepository { get; set; }

        /// <summary>
        /// Passes a user instance onto the <see cref="UserRepository"/> in order to have it created in the database.
        /// </summary>
        /// <param name="user">A new user to add.</param>
        public void CreateEntity(USER user)
        {
            user.USERID = this.UserRepository.GetLastId() + 1;
            this.UserRepository.CreateEntity(user);
        }

        /// <summary>
        /// Deletes a user instance.
        /// </summary>
        /// <param name="id">The id of the user to delete.</param>
        public void DeleteEntity(int id)
        {
            this.UserRepository.DeleteEntity(id);
        }

        /// <summary>
        /// Updates a user of a given id.
        /// </summary>
        /// <param name="oldUsersId">The id of the user.</param>
        /// <param name="user">The user object.</param>
        public void UpdateEntity(int oldUsersId, USER user)
        {
            this.UserRepository.UpdateEntity(oldUsersId, user);
        }

        /// <summary>
        /// Returns a User object of the specified id.
        /// </summary>
        /// <param name="id">The id of the desired user.</param>
        /// <returns>USER.</returns>
        public USER GetEntityById(int id)
        {
            return this.UserRepository.GetEntity(id);
        }

        /// <summary>
        /// Returns all User elements found in the database.
        /// </summary>
        /// <returns>An users collection.</returns>
        public IList<USER> GetAllEntities()
        {
            return this.UserRepository.GetAllEntities().ToList();
        }

        /// <summary>
        /// Returns the user entity with the most number of reviews posted.
        /// </summary>
        /// <returns>A user entity.</returns>
        public USER GetUserWithMostReviewsAndHighestRatings()
        {
            var collection = this.UserRepository.GetAllEntities();
            var getData = (from x in collection
                           let maxReviewNumber =
                             (from y in collection
                              select new
                              {
                                  ID = y.USERID,
                                  ReviewNumber = y.REVIEWS.Count,
                                  AvgRating = y.AVERAGE_RATING_GIVEN,
                              }).OrderByDescending(t => t.ReviewNumber).ThenByDescending(z => z.AvgRating).FirstOrDefault()
                           where x.USERID == maxReviewNumber.ID
                           select x).FirstOrDefault();
            return getData;
        }
    }
}
