﻿// <copyright file="Test.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyMDB.Data;
    using MyMDB.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Creates a basic setup environment for further tester classes to inherit, thus providing scaling advantages.
    /// </summary>
    [TestFixture]
    public abstract class Test
    {
        /// <summary>
        /// Gets or sets an <see cref="IMovieLogic"/> implementation to test.
        /// </summary>
        public virtual IMovieLogic MovieLogic { get; set; }

        /// <summary>
        /// Gets or sets an <see cref="IActorLogic"/> implementation to test.
        /// </summary>
        public IActorLogic ActorLogic { get; set; }

        /// <summary>
        /// Gets or sets an <see cref="IUserLogic"/> implementation to test.
        /// </summary>
        public IUserLogic UserLogic { get; set; }

        /// <summary>
        /// Gets or sets an <see cref="IReviewLogic"/> implementation to test.
        /// </summary>
        public IReviewLogic ReviewLogic { get; set; }

        /// <summary>
        /// Creates mocked repositories for all the available logic types.
        /// </summary>
        [SetUp]
        public virtual void Setup()
        {
            this.SetUpActorMockedRepo();
            this.SetUpMovieMockedRepo();
            this.SetUpUserMockedRepo();
        }

        private void SetUpMovieMockedRepo()
        {
            var mock = new Mock<IRepository<MOVIE>>();
            List<MOVIE> testSet = new List<MOVIE>();

            testSet.Add(new MOVIE()
            {
                MOVIEID = 1,
                TITLE = "TestMovie1",
                GENRE = "GG",
                RELEASED = DateTime.Parse("2000-01-01"),
                AGE_LIMIT = 6,
                RATING = 6.0m,
                ACTORS = this.ActorLogic.GetAllEntities(),
            });
            testSet.Add(new MOVIE()
            {
                MOVIEID = 2,
                TITLE = "TestMovie2",
                GENRE = "gg",
                RELEASED = DateTime.Parse("2020-01-01"),
                AGE_LIMIT = 66,
                RATING = 6.6m,
            });
            testSet.Add(new MOVIE()
            {
                MOVIEID = 3,
                TITLE = "TestMovie3",
                GENRE = "TestGenre",
                RELEASED = DateTime.Parse("2220-01-01"),
                AGE_LIMIT = 14,
                RATING = 5.6M,
            });
            mock.Setup(t => t.GetAllEntities()).Returns(testSet.AsQueryable);
            mock.Setup(t => t.CreateEntity(It.IsAny<MOVIE>())).Callback((MOVIE m) => testSet.Add(m));
            this.MovieLogic = new MovieLogic();
            this.MovieLogic.MovieRepository = mock.Object;
        }

        private void SetUpActorMockedRepo()
        {
            var mock = new Mock<IRepository<ACTOR>>();
            List<ACTOR> testSet = new List<ACTOR>();
            MOVIE testMovie = new MOVIE() { MOVIEID = 99, TITLE = "Actor Tester Movie", DIRECTOR = "Direct Thor" };
            List<MOVIE> testMovieCollection = new List<MOVIE>();
            testMovieCollection.Add(testMovie);

            testSet.Add(new ACTOR()
            {
                ACTORID = 1,
                NAME = "TestActor1",
                BIRTH_DATE = DateTime.Parse("1999.09.09"),
                OSCAR_NOMINATIONS = 5,
                OSCAR_WINS = 1,
                TOTAL_AWARD_WINS = 3,
                TRADEMARK = "First actor",
                MOVIES = testMovieCollection,
            });
            testSet.Add(new ACTOR()
            {
                ACTORID = 2,
                NAME = "TestActor2",
                BIRTH_DATE = DateTime.Parse("1998.08.08"),
                OSCAR_NOMINATIONS = 3,
                OSCAR_WINS = 0,
                TOTAL_AWARD_WINS = 1,
                TRADEMARK = "Second actor",
                MOVIES = testMovieCollection,
            });
            testSet.Add(new ACTOR()
            {
                ACTORID = 3,
                NAME = "TestActor3",
                BIRTH_DATE = DateTime.Parse("1970.01.01"),
                OSCAR_NOMINATIONS = 15,
                OSCAR_WINS = 3,
                TOTAL_AWARD_WINS = 25,
                TRADEMARK = "Third actor",
                MOVIES = testMovieCollection,
            });

            mock.Setup(t => t.GetAllEntities()).Returns(testSet.AsQueryable);
            this.ActorLogic = new ActorLogic();
            this.ActorLogic.ActorRepository = mock.Object;
        }

        private void SetUpUserMockedRepo()
        {
            var mock = new Mock<IRepository<USER>>();
            List<USER> testSet = new List<USER>();

            testSet.Add(new USER()
            {
                USERNAME = "Teszt Elek",
                NUMBER_OF_REVIEWS = 5,
                AVERAGE_RATING_GIVEN = 9.0M,
            });

            testSet.Add(new USER()
            {
                USERNAME = "Inferior Teszt Elek",
                NUMBER_OF_REVIEWS = 2,
                AVERAGE_RATING_GIVEN = 9.5M,
            });

            mock.Setup(t => t.GetAllEntities()).Returns(testSet.AsQueryable);
            this.UserLogic = new UserLogic();
            this.UserLogic.UserRepository = mock.Object;
        }
    }
}
