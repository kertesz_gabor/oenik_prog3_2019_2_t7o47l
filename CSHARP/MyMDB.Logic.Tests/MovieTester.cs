﻿// <copyright file="MovieTester.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyMDB.Data;
    using MyMDB.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests an <see cref="IMovieLogic"/> implementation.
    /// </summary>
    [TestFixture]
    public class MovieTester : Test
    {
        /// <summary>
        /// Tests whether the current <see cref="IRepository{T}"/> implementation throws certain <see cref="Exception"/>s upon deletion or not.
        /// </summary>
        [Test]
        public void DeleteTest()
        {
            Assert.That(() => this.MovieLogic.DeleteEntity(-5), Throws.TypeOf<ArgumentException>());
            Assert.That(() => this.MovieLogic.DeleteEntity(20), Throws.TypeOf<KeyNotFoundException>());
        }

        /// <summary>
        /// Tests if upon addition, a certain <see cref="MOVIE"/> entity can be retrieved from the database or not.
        /// </summary>
        [Test]
        public void AddTest()
        {
            this.MovieLogic.CreateEntity(new MOVIE { TITLE = "ADDED DATA" });
            Assert.That(() => this.MovieLogic.GetAllEntities()[3].TITLE.Equals("ADDED DATA"));
        }

        /// <summary>
        /// Tests the average rating per genre functionality in the program.
        /// </summary>
        [Test]
        public void GenreAverageRatingTest()
        {
            Assert.That(this.MovieLogic.GetAverageRatingPerGenre().FirstOrDefault().Contains("GG"));
        }

        /// <summary>
        /// Tests the top 5 movies listing functionality in the program.
        /// </summary>
        [Test]
        public void Top5MoviesTest()
        {
            Assert.That(this.MovieLogic.GetTop5Movies().FirstOrDefault().TITLE.Equals("TestMovie2"));
        }

        /// <summary>
        /// Tests the top 5 movies listing functionality in the program.
        /// </summary>
        [Test]
        public void AllActorsInAMovie()
        {
            Assert.That(this.MovieLogic.GetMoviesInWhichAllActorsPlay(this.ActorLogic.GetAllEntities().ToList()).FirstOrDefault().MOVIEID == 1);
        }
    }
}
