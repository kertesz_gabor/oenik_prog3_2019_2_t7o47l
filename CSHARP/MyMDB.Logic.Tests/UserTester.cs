﻿// <copyright file="UserTester.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyMDB.Data;
    using MyMDB.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests an <see cref="IUserLogic"/> implementation.
    /// </summary>
    [TestFixture]
    public class UserTester : Test
    {
        /// <summary>
        /// Tests the actors and their favourite director function of the program.
        /// </summary>
        [Test]
        public void GetUserWithMostReviewsAndHighestRatingsTest()
        {
            Assert.That(this.UserLogic.GetUserWithMostReviewsAndHighestRatings().USERNAME.Equals("Teszt Elek"));
        }
    }
}
