﻿// <copyright file="ActorTester.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyMDB.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyMDB.Data;
    using MyMDB.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests an <see cref="IActorLogic"/> implementation.
    /// </summary>
    [TestFixture]
    public class ActorTester : Test
    {
        /// <summary>
        /// Tests whether the current <see cref="IRepository{T}"/> implementation throws certain <see cref="Exception"/>s upon getting an element by id or not.
        /// </summary>
        [Test]
        public void GetByIdTest()
        {
            Assert.That(() => this.ActorLogic.GetEntityById(-5), Throws.TypeOf<ArgumentException>());
            Assert.That(() => this.ActorLogic.GetEntityById(20), Throws.TypeOf<KeyNotFoundException>());
        }

        /// <summary>
        /// Tests the GetAll functionality provided by the <see cref="ILogic{T}"/> interface.
        /// </summary>
        [Test]
        public void GetAllTest()
        {
            var getAllEntities = this.ActorLogic.GetAllEntities();
            Assert.That(getAllEntities[0].NAME.Equals("TestActor1"));
            Assert.That(getAllEntities[1].NAME.Equals("TestActor2"));
            Assert.That(getAllEntities[2].NAME.Equals("TestActor3"));
        }

        /// <summary>
        /// Tests the actors and their favourite director function of the program.
        /// </summary>
        [Test]
        public void ActorsFavDirectorsTest()
        {
            Assert.That(this.ActorLogic.GetAllActorsFavDirectors().FirstOrDefault().Contains("TestActor1"));
            Assert.That(this.ActorLogic.GetAllActorsFavDirectors().FirstOrDefault().Contains("Direct Thor"));
        }
    }
}
