﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides the frontend platform of the application.
    /// </summary>
    public static class Menu
    {
        /// <summary>
        /// Initalizes a new menu. Provides options for the user to choose from.
        /// </summary>
        /// <returns>The number of the chosen submenu.</returns>
        public static byte InitMenu()
        {
            Console.Clear();
            DisplayTopMenu();
            try
            {
                var choice = ReadUserChoice();
                return choice;
            }
            catch (FormatException)
            {
                Console.Clear();
                Console.WriteLine("Hibás választás, kérlek próbáld újból!");
                Console.ReadKey();
                return InitMenu();
            }
        }

        /// <summary>
        /// Displays a basic menu on screen.
        /// </summary>
        /// <returns>The chosen submenu option.</returns>
        /// <param name="menuItems">The list of menu items to show.</param>
        public static int DisplaySubMenu(List<string> menuItems)
        {
            Console.Clear();
            int choiceNumber = 0;
            foreach (var item in menuItems)
            {
                Console.WriteLine(string.Format("\t({0}) - {1}", ++choiceNumber, item));
            }

            return ReadUserChoice(true); // indicate that it is indeed a submenu we are going to display.
        }

        /// <summary>
        /// Displays a gicen collection on the console window.
        /// </summary>
        /// <typeparam name="T">The type of the elements in the collection.</typeparam>
        /// <param name="collection">The collection to display.</param>
        public static void DisplayCollection<T>(ICollection<T> collection)
        {
            Console.Clear();
            var currentType = typeof(T);
            var properties = currentType.GetProperties();
            var isString = currentType.Name.Equals("String");
            if (!isString)
            {
                GenerateHeader<T>();
            }

            Console.WriteLine();
            foreach (var item in collection)
            {
                if (isString)
                {
                    Console.WriteLine(item);
                }
                else
                {
                    foreach (var prop in properties)
                    {
                        if (!prop.PropertyType.Name.Contains("ICollection`1"))
                        {
                            if (prop.PropertyType.FullName.Contains("Date"))
                            {
                                Console.Write(prop.GetValue(item, null).ToString().Split(' ')[0]);
                            }
                            else
                            {
                                Console.Write(prop.GetValue(item, null));
                            }
                        }

                        Console.Write("\t        ");
                    }
                }

                Console.Write('\n');
            }
        }

        /// <summary>
        /// Defines a basic template for adding an entity of any type to the database.
        /// </summary>
        /// <typeparam name="T">Any type of entity.</typeparam>
        /// <param name="sender">Provides an opportunity for accessing other logic operations.</param>
        /// <returns>An entity to add.</returns>
        public static T EntityAdder<T>(Program.ProgramInstance sender)
        {
            var currentType = typeof(T);
            var entity = (T)Activator.CreateInstance(currentType);
            Console.Clear();
            Console.WriteLine($"Új {currentType.Name} típusú elem rögzítése az adatbázisban\n");
            Console.WriteLine("Kérlek, add meg az alábbi adatokat!");
            foreach (var prop in currentType.GetProperties())
            {
                if (!prop.PropertyType.FullName.Contains("Collection") && !prop.Name.ToLower().Contains("id"))
                {
                    Console.Write($"Kérem a(z) {prop.Name} paramétert!");

                    if (prop.PropertyType.Name == "MOVIE")
                    {
                        Console.WriteLine("Kérem az értékelt film id-ját!");
                        var getMovie = sender.MovieLogic.GetEntityById(Menu.IdChooser());
                        prop.SetValue(entity, getMovie, null);
                    }
                    else if (prop.PropertyType.Name == "USER")
                    {
                        Console.WriteLine("Kérem az értékelő felhasználó id-ját!");
                        var getUser = sender.UserLogic.GetEntityById(Menu.IdChooser());
                        prop.SetValue(entity, getUser, null);
                    }
                    else
                    {
                        var userInput = Console.ReadLine();
                        if (prop.PropertyType.Name == "String")
                        {
                            prop.SetValue(entity, userInput, null);
                        }
                        else
                        {
                            MethodInfo getParsingMethod = null;
                            if (prop.PropertyType.FullName.Contains("DateTime"))
                            {
                                getParsingMethod = typeof(DateTime).GetMethods().Where(t => t.Name.Contains("Parse")).FirstOrDefault();
                            }
                            else if (prop.PropertyType.FullName.Contains("Decimal"))
                            {
                                getParsingMethod = typeof(decimal).GetMethods().Where(t => t.Name.Contains("Parse")).FirstOrDefault();
                            }
                            else
                            {
                                getParsingMethod = prop.PropertyType.GetMethods().Where(t => t.Name.Contains("Parse")).FirstOrDefault();
                            }

                            var getParsedData = getParsingMethod.Invoke(null, new object[] { userInput });
                            prop.SetValue(entity, getParsedData, null);
                        }
                    }

                    Console.Write('\n');
                }
            }

            return entity;
        }

        /// <summary>
        /// Updates an entity in the database.
        /// </summary>
        /// <typeparam name="T">Any entity type.</typeparam>
        /// <param name="id">The id of the desired element.</param>
        /// <param name="entities">The entities of the database to choose from.</param>
        /// <param name="sender">Required parameter to pass through to <see cref="EntityAdder{T}(Program.ProgramInstance)"/>.</param>
        /// <returns>A record with the type of the entity.</returns>
        public static T EntityUpdater<T>(out int id, ICollection<T> entities, object sender)
        {
            Console.Clear();
            DisplayCollection<T>(entities);
            id = Menu.IdChooser();
            Console.Clear();
            return EntityAdder<T>((Program.ProgramInstance)sender);
        }

        /// <summary>
        /// Provides a platform to help delete an entity from the database.
        /// </summary>
        /// <typeparam name="T">Any entity tpye.</typeparam>
        /// <param name="entities">The entities of the database to choose from.</param>
        /// <returns>The id of the entity to delete.</returns>
        public static int EntityDeleter<T>(ICollection<T> entities)
        {
            Console.Clear();
            DisplayCollection<T>(entities);
            return Menu.IdChooser();
        }

        /// <summary>
        /// Provides a solution for ID choosing.
        /// </summary>
        /// <returns>The chosen id.</returns>
        public static byte IdChooser()
        {
            Console.WriteLine("\nKérlek add meg a választott elem ID-ját!");
            return byte.Parse(Console.ReadLine());
        }

        private static void DisplayTopMenu()
        {
            Console.WriteLine("\t*****************************************************");
            Console.WriteLine("\t*(1) - Filmek kezelése                              *");
            Console.WriteLine("\t*(2) - Színészek kezelése                           *");
            Console.WriteLine("\t*(3) - Felhasználók kezelése                        *");
            Console.WriteLine("\t*(4) - Értékelések kezelése                         *");
            Console.WriteLine("\t*(5) - Film átadása a webes felület számára         *");
            Console.WriteLine("\t*(6) - Kilépés a programból                         *");
            Console.WriteLine("\t*****************************************************");
        }

        private static byte ReadUserChoice()
        {
            Console.WriteLine("\nKérlek, válassz a fenti lehetőségek közül!");
            char readKey = Console.ReadKey().KeyChar;
            return byte.Parse(readKey.ToString());
        }

        private static byte ReadUserChoice(bool isSubMenu)
        {
            Console.WriteLine("\nKérlek, válassz a fenti lehetőségek közül, vagy nyomj ESC-et a visszalépéshez!");
            ConsoleKeyInfo readKey = Console.ReadKey();
            if (readKey.Key == ConsoleKey.Escape)
            {
                return 0;
            }
            else
            {
                return byte.Parse(readKey.KeyChar.ToString());
            }
        }

        private static void GenerateHeader<T>()
        {
            var currentType = typeof(T);
            var properties = currentType.GetProperties();
            foreach (var item in properties)
            {
                if (!item.PropertyType.Name.Contains("ICollection`1"))
                {
                    Console.Write(item.Name + "\t\t");
                }
            }
        }
    }
}
