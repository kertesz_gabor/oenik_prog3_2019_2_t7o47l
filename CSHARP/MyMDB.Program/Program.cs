﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using MyMDB.Data;
    using MyMDB.Logic;

    /// <summary>
    /// Initilizes a console window.
    /// </summary>
    public class Program
    {
        private const string JavaUrlTemplate = "http://localhost:8080/JAVA/MovieQuery?";

        /// <summary>
        /// Creates a new <see cref="ProgramInstance"/>, and starts the program.
        /// </summary>
        /// <param name="args">Starting arguments.</param>
        public static void Main(string[] args)
        {
            try
            {
                Console.WindowWidth = 200;
                ProgramInstance program = new ProgramInstance();
                program.StartProgram();
                Console.ReadKey();
            }
            catch (System.ArgumentOutOfRangeException)
            {
                // hagyjuk békén a console méretét...
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            catch (KeyNotFoundException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            catch (OverflowException)
            {
                Console.WriteLine("Hibásan adtad meg az adatot!");
                Console.ReadLine();
            }
            finally
            {
                Main(null);
            }
        }

        /// <summary>
        /// Creates a new platform for the console window to interact with.
        /// </summary>
        public class ProgramInstance
        {
            private IMovieLogic movieLogic;
            private IActorLogic actorLogic;
            private IUserLogic userLogic;
            private IReviewLogic reviewLogic;

            /// <summary>
            /// Gets or sets an instance of the type <see cref="IMovieLogic"/>.
            /// </summary>
            public IMovieLogic MovieLogic { get => this.movieLogic; set => this.movieLogic = value; }

            /// <summary>
            /// Gets or sets an instance of the type <see cref="IUserLogic"/>.
            /// </summary>
            public IUserLogic UserLogic { get => this.userLogic; set => this.userLogic = value; }

            /// <summary>
            /// Handles the running of the main program.
            /// </summary>
            public void StartProgram()
            {
                this.MovieLogic = new MovieLogic();
                this.actorLogic = new ActorLogic();
                this.UserLogic = new UserLogic();
                this.reviewLogic = new ReviewLogic();

                bool programShouldBeRunning = true;
                do
                {
                    var chosenMenu = Menu.InitMenu();
                    switch (chosenMenu)
                    {
                        case 1: this.HandleMovies(); break;
                        case 2: this.HandleActors(); break;
                        case 3: this.HandleUsers(); break;
                        case 4: this.HandleReviews(); break;
                        case 5: this.CallJava(); break;
                        case 6: Environment.Exit(0); break;
                        default:
                            break;
                    }
                }
                while (programShouldBeRunning);
            }

            private void HandleMovies()
            {
                bool stayInCurrentMenu = true;
                while (stayInCurrentMenu)
                {
                    int chosenOption = Menu.DisplaySubMenu(new string[]
                    {
                        "Filmek listázása", "Új film hozzáadása", "Film frissítése", "Film törlése",
                        "Az öt legmagasabb értékelésű film listázása", "Színészek közös filmjeinek listázása", "Zsánerenkénti átlagértékelések listázása",
                    }.ToList());
                    int id = 0;

                    switch (chosenOption)
                    {
                        case 1: Menu.DisplayCollection(this.MovieLogic.GetAllEntities()); Console.ReadKey(); break;
                        case 2: this.MovieLogic.CreateEntity(Menu.EntityAdder<MOVIE>(null)); break;
                        case 3: MOVIE updated = Menu.EntityUpdater(out id, this.MovieLogic.GetAllEntities(), null); this.MovieLogic.UpdateEntity(id, updated); break;
                        case 4: this.MovieLogic.DeleteEntity(Menu.EntityDeleter<MOVIE>(this.MovieLogic.GetAllEntities())); break;
                        case 5: Menu.DisplayCollection(this.MovieLogic.GetTop5Movies()); Console.ReadKey(); break;
                        case 6: Menu.DisplayCollection(this.MovieLogic.GetMoviesInWhichAllActorsPlay(this.ActorPicker())); Console.ReadKey(); break;
                        case 7: Menu.DisplayCollection(this.MovieLogic.GetAverageRatingPerGenre()); Console.ReadKey(); break;
                        default: stayInCurrentMenu = false;
                            break;
                    }
                }
            }

            private List<ACTOR> ActorPicker()
            {
                Menu.DisplayCollection(this.actorLogic.GetAllEntities());
                Console.WriteLine("Kérlek, hogy vesszővel elválasztva add meg azokat a színészeket, akiknek a közös filmjeit szeretnéd listázni:");
                string[] pickedActorsID = Console.ReadLine().Split(',');
                List<ACTOR> pickedActors = new List<ACTOR>();
                for (int i = 0; i < pickedActorsID.Length; i++)
                {
                    pickedActors.Add(this.actorLogic.GetEntityById(int.Parse(pickedActorsID[i])));
                }

                return pickedActors;
            }

            private void HandleUsers()
            {
                bool stayInCurrentMenu = true;
                while (stayInCurrentMenu)
                {
                    int chosenOption = Menu.DisplaySubMenu(new string[]
                    {
                        "Felhasználók listázása", "Új felhasználó hozzáadása", "Felhasználó frissítése", "Felhasználó törlése", "Legtöbb értékeléssel és legmagasabb átlaggal rendelkező felhasználó",
                    }.ToList());
                    int id = 0;
                    switch (chosenOption)
                    {
                        case 1: Menu.DisplayCollection(this.UserLogic.GetAllEntities()); Console.ReadKey(); break;
                        case 2: this.UserLogic.CreateEntity(Menu.EntityAdder<USER>(null)); break;
                        case 3: USER updated = Menu.EntityUpdater(out id, this.UserLogic.GetAllEntities(), null); this.UserLogic.UpdateEntity(id, updated); break;
                        case 4: this.UserLogic.DeleteEntity(Menu.EntityDeleter<USER>(this.UserLogic.GetAllEntities())); break;
                        case 5: Console.WriteLine('\n' + this.userLogic.GetUserWithMostReviewsAndHighestRatings().USERNAME); Console.ReadKey(); break;
                        default:
                            stayInCurrentMenu = false;
                            break;
                    }
                }
            }

            private void HandleReviews()
            {
                bool stayInCurrentMenu = true;
                while (stayInCurrentMenu)
                {
                    int chosenOption = Menu.DisplaySubMenu(new string[]
                    {
                        "Értékelések listázása", "Új értékelés hozzáadása", "Értékelés frissítése", "Értékelés törlése",
                    }.ToList());
                    int id = 0;
                    switch (chosenOption)
                    {
                        case 1: Menu.DisplayCollection(this.reviewLogic.GetAllEntities()); Console.ReadKey(); break;
                        case 2: this.reviewLogic.CreateEntity(Menu.EntityAdder<REVIEW>(this)); break;
                        case 3: REVIEW updated = Menu.EntityUpdater(out id, this.reviewLogic.GetAllEntities(), this); this.reviewLogic.UpdateEntity(id, updated); break;
                        case 4: this.reviewLogic.DeleteEntity(Menu.EntityDeleter<REVIEW>(this.reviewLogic.GetAllEntities())); break;
                        default:
                            stayInCurrentMenu = false;
                            break;
                    }
                }
            }

            private void HandleActors()
            {
                bool stayInCurrentMenu = true;
                while (stayInCurrentMenu)
                {
                    int chosenOption = Menu.DisplaySubMenu(new string[]
                    {
                        "Színészek listázása", "Új színész hozzáadása", "Színész frissítése", "Színész törlése", "Színészek és a kedvenc rendezőik",
                    }.ToList());
                    int id = 0;
                    switch (chosenOption)
                    {
                        case 1: Menu.DisplayCollection(this.actorLogic.GetAllEntities()); Console.ReadKey(); break;
                        case 2: this.actorLogic.CreateEntity(Menu.EntityAdder<ACTOR>(null)); break;
                        case 3: ACTOR updated = Menu.EntityUpdater(out id, this.actorLogic.GetAllEntities(), null); this.actorLogic.UpdateEntity(id, updated); break;
                        case 4: this.actorLogic.DeleteEntity(Menu.EntityDeleter<ACTOR>(this.actorLogic.GetAllEntities())); break;
                        case 5: Menu.DisplayCollection(this.actorLogic.GetAllActorsFavDirectors()); Console.ReadKey(); break;
                        default:
                            stayInCurrentMenu = false;
                            break;
                    }
                }
            }

            private void CallJava()
            {
                Console.Clear();
                Console.WriteLine("\t\tWebes végpont kezelő");
                Menu.DisplayCollection(this.MovieLogic.GetAllEntities());
                MOVIE getMovie = this.MovieLogic.GetEntityById(Menu.IdChooser());
                var webResponse = this.ParseXMLResponse(this.GenerateRequestURL(getMovie));
                Menu.DisplayCollection(webResponse);
                Console.ReadKey();
            }

            private string GenerateRequestURL(MOVIE movie)
            {
                StringBuilder url = new StringBuilder();
                url.Append(JavaUrlTemplate);
                url.Append($"title={movie.TITLE}&");
                url.Append($"language={movie.LANGUAGE}&");
                url.Append($"released={movie.RELEASED}&");
                url.Append($"genre={movie.GENRE}&");
                url.Append($"rating={movie.RATING}&");
                url.Append($"director={movie.DIRECTOR}&");
                url.Append($"agelimit={movie.AGE_LIMIT}&");
                url.Append($"writers={movie.WRITERS}");
                return url.ToString();
            }

            private List<string> ParseXMLResponse(string url)
            {
                XDocument response = XDocument.Load(url);
                List<string> result = new List<string>();
                var movie = response.Descendants("movie").FirstOrDefault();
                result.Add($"Title:\t\t{movie.Element("title").Value}");
                result.Add($"Language:\t{movie.Element("language").Value}");
                result.Add($"Released:\t{movie.Element("released").Value}");
                result.Add($"Genre:\t\t{movie.Element("genre").Value}");
                result.Add($"Rating:\t\t{movie.Element("rating").Value}");
                result.Add($"Director:\t{movie.Element("director").Value}");
                result.Add($"Age limit:\t{movie.Element("ageLimit").Value}");
                result.Add($"Writers:\t{movie.Element("writers").Value}");
                result.Add($"Hash code:\t{movie.Element("hashCode").Value}");
                return result;
            }
        }
    }
}
