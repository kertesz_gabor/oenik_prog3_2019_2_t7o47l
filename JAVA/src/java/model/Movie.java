/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;
import java.util.Random;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kertész Gábor
 */
@XmlRootElement  
public class Movie {
    private int movieID;
    private String  title; 
    private String language;
    private String genre;
    private String released;
    private String director;
    private double rating;
    private int ageLimit;
    private String writers;
    private int hashCode;

    public Movie() {
        
    }


    public int getMovieID() {
        return movieID;
    }

    @XmlElement  
    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    @XmlElement
    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @XmlElement
    public int getAgeLimit() {
        return ageLimit;
    }

    public void setAgeLimit(int ageLimit) {
        this.ageLimit = ageLimit;
    }

    @XmlElement
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    @XmlElement
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @XmlElement
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @XmlElement
    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @XmlElement
    public String getWriters() {
        return writers;
    }

    public void setWriters(String writers) {
        this.writers = writers;
    }

    @XmlElement
    public int getHashCode() {
        return hashCode;
    }

    @Override
    public int hashCode() {
        Random r = new Random();
        int hash = 7;
        hash = r.nextInt(10) * hash + this.movieID;
        hash = r.nextInt(10) * hash + Objects.hashCode(this.released);
        hash = r.nextInt(10) * hash + (int) (Double.doubleToLongBits(this.rating) ^ (Double.doubleToLongBits(this.rating) >>> 32));
        hash = r.nextInt(10) * hash + this.ageLimit;
        hash = r.nextInt(10) * hash + Objects.hashCode(this.title);
        hash = r.nextInt(10) * hash + Objects.hashCode(this.language);
        hash = r.nextInt(10) * hash + Objects.hashCode(this.genre);
        hash = r.nextInt(10) * hash + Objects.hashCode(this.director);
        hash = r.nextInt(10) * hash + Objects.hashCode(this.writers);
        this.hashCode = hash;
        return hash;
    }

   

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movie other = (Movie) obj;
        if (this.movieID != other.movieID) {
            return false;
        }
        return true;
    }

    
    
    
    
}
