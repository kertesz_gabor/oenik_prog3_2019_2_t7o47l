package controller;

import java.io.FileOutputStream;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import model.Movie;



 public class Logic {
    public static String GenerateXMLOutput(Movie movie) throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(Movie.class); 
        
        Marshaller marshall = context.createMarshaller();
        marshall.setProperty(marshall.JAXB_FORMATTED_OUTPUT, true);
        
        StringWriter output = new StringWriter();
        marshall.marshal(movie, output);
        return output.toString();
    }
}
